---
layout: page
title: Blog
description: The latest news and updates.
permalink: "/blog/"
---

<ul>
  {% for post in site.posts %}
    <li>
      <h3><a href="{{ post.url }}">{{ post.title }}</a></h3>
      {{ post.description }}
    </li>
  {% endfor %}
</ul>
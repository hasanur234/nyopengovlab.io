---
layout: page
title: Request Help
description: If you are having a problem dealing with an open government issue, we
  might be able to help with information or advice.
---

## Help Request Form

Please fill out the form below and we will respond to you as soon as possible.

<!-- Form -->
<section>
	<form method="post" action="https://formspree.io/info@nyopengov.org">
		<div class="row uniform">
			<div class="6u 12u$(xsmall)">
				<input type="text" name="name" id="demo-name" value="" placeholder="Name" />
			</div>
			<div class="6u$ 12u$(xsmall)">
				<input type="email" name="_replyto" id="demo-email" value="" placeholder="Email" />
			</div>
			<div class="12u$">
				<div class="select-wrapper">
					<select name="category" id="demo-category">
						<option value="">- Category -</option>
						<option value="1">Option One</option>
						<option value="1">Option Two</option>
						<option value="1">Option Three</option>
						<option value="1">Option Four</option>
					</select>
				</div>
			</div>
			<div class="4u 12u$(small)">
				<input type="radio" id="demo-priority-low" name="priority" checked>
				<label for="demo-priority-low">Low</label>
			</div>
			<div class="4u 12u$(small)">
				<input type="radio" id="demo-priority-normal" name="priority">
				<label for="demo-priority-normal">Normal</label>
			</div>
			<div class="4u$ 12u$(small)">
				<input type="radio" id="demo-priority-high" name="priority">
				<label for="demo-priority-high">High</label>
			</div>
			<div class="12u$">
				<textarea name="message" id="demo-message" placeholder="Enter your message" rows="6"></textarea>
				<input type="hidden" name="_subject" value="HELP REQUEST" />
				<input type="text" name="_gotcha" style="display:none" />
			</div>
			<div class="12u$">
				<ul class="actions">
					<li><input type="submit" value="Send Message" class="special" /></li>
					<li><input type="reset" value="Reset" /></li>
				</ul>
			</div>
		</div>
	</form>
</section>
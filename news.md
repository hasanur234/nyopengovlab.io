---
title: News
layout: page
description: BNCOG in the News
---
* [http://www.niagara-gazette.com/news/local_news/local-group-reports-on-open-government-during-sunshine-week/article_321ac56d-f34d-5e49-878f-6a0a7f516bfe.html](http://www.niagara-gazette.com/news/local_news/local-group-reports-on-open-government-during-sunshine-week/article_321ac56d-f34d-5e49-878f-6a0a7f516bfe.html "http://www.niagara-gazette.com/news/local_news/local-group-reports-on-open-government-during-sunshine-week/article_321ac56d-f34d-5e49-878f-6a0a7f516bfe.html")
* [http://www.niagara-gazette.com/news/local_news/buffalo-niagara-coalition-for-open-government-president-discusses-push-for/article_fd6f9eff-1acc-5f48-8613-eda3c9234db7.html](http://www.niagara-gazette.com/news/local_news/buffalo-niagara-coalition-for-open-government-president-discusses-push-for/article_fd6f9eff-1acc-5f48-8613-eda3c9234db7.html "http://www.niagara-gazette.com/news/local_news/buffalo-niagara-coalition-for-open-government-president-discusses-push-for/article_fd6f9eff-1acc-5f48-8613-eda3c9234db7.html")
* [http://news.wbfo.org/post/many-local-governments-get-poor-grades-transparency](http://news.wbfo.org/post/many-local-governments-get-poor-grades-transparency "http://news.wbfo.org/post/many-local-governments-get-poor-grades-transparency")
* [Executive Sessions Report, October 25th 2017](https://www.scribd.com/document/362796385/Executive-Merged-Report)
* [Website Report March 15, 2017](http://nyopengov.org/Coalition_Website_Report.pdf)
* [FOIL Report - May 17, 2017](FOIL_report-2017-5-17.pdf)
* [http://buffalonews.com/2017/05/19/editorial-officials-duty-preserve-official-records/](http://buffalonews.com/2017/05/19/editorial-officials-duty-preserve-official-records/)
* [http://buffalonews.com/2017/05/17/amherst-denies-foil-request-weinsteins-calendar-supervisor-says-threw-away/](http://buffalonews.com/2017/05/17/amherst-denies-foil-request-weinsteins-calendar-supervisor-says-threw-away/)
* [http://buffalonews.com/2016/06/22/buffalo-niagara-coaltion-for-open-government-badly-needed-in-western-new-york/](http://buffalonews.com/2016/06/22/buffalo-niagara-coaltion-for-open-government-badly-needed-in-western-new-york/)
* [http://news.wbfo.org/post/attorney-seeks-shine-light-local-government](http://news.wbfo.org/post/attorney-seeks-shine-light-local-government)
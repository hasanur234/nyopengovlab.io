---
layout: page
title: About
description: Learn about who we are, how we started, and how you can help.
---

The Buffalo Niagara Coalition for Open Government is a nonpartisan organization comprised of journalists, activists, attorneys, educators, news media organizations, and other concerned citizens who value open government and freedom of information.

## Mission Statement

Through advocacy, education and civic engagement, the Buffalo Niagara Coalition for Open Government promotes open, transparent government and defends citizens’ right to access information from public institutions at the city, county, and state levels.

## Statement of Purpose

We believe that, if government is of the people, by the people and for the people, then it should also be open TO the people. Government exists to serve its citizens, so access to public information should be simple. Freedom of Information Laws and the NY Open Meetings Law make access to public records a right.

When government operates openly and honestly, we, the people, can hold our elected officials accountable, fulfilling our duties as an informed citizenry. The Buffalo Niagara Coalition for Open Government works to ensure that all people have full access to government records and proceedings on the city, county, and state levels. Such access fosters responsive, accountable government, stimulates civic involvement and builds trust in government.

In order to accomplish our Mission, the Buffalo Coalition for Open Government:

- Offers a regional, collective voice on open government issues.
- Monitors the legislature, state agencies, courts and local governments for open-government actions and issues.
- Fosters the open exchange of information through networking, seminars, training sessions and conferences.
- Works to start up new, and support existing, open government boards at the city, town, or village level.
- Explores transparency and information-sharing issues, making recommendations for either state or local government action.
- Counsels and educates public officials on their legal responsibilities around open government and transparency.
- Assists with Court actions in support of greater government transparency.
- Prepares educational reports or other publications.
Shares information through our Blog, newsfeed and online resources.